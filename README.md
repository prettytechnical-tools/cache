# Cache

**Cache** This package basically provides us with an interface to communicate with Redis and thus manage the cache without using repetitive logic. That way, the logic is abstracted and we only focus on the functionality.

![Go](https://img.shields.io/badge/Golang-1.21-blue.svg?logo=go&longCache=true&style=flat)

This package makes use of the external package [redis](github.com/go-redis/redis) and our [logger](gitlab.com/prettytechnical-tools/logger) package.

## Getting Started

[Go](https://golang.org/) is required in version 1.21 or higher.

### Install

`go get -u gitlab.com/prettytechnical-tools/cache`

### Features

* [x] **Lightweight**, less than 200 lines of code.
* [x] **Easy** to use.

### Basic use

```go
package main

import (
	"fmt"
	"log"

	"gitlab.com/prettytechnical-tools/cache"
)

func main() {
	var (
		redisHost = "localhost:6379"
		redisPassword = ""
	)
	c := cache.NewRedisClient(redisHost, redisPassword)
	value, err := c.Get("key")
	if err != nil {
		//manage the error
		log.Fatal(err)
	}
	
	//do something with the value
	fmt.Println(string(value))
}



```