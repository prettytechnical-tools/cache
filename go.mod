module gitlab.com/prettytechnical-tools/cache

go 1.21.1

require (
	github.com/go-redis/redis v6.15.9+incompatible
	gitlab.com/prettytechnical-tools/logger v1.0.0
)

require (
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.30.0 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	go.uber.org/zap v1.26.0 // indirect
)
