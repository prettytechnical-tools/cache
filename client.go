package cache

import (
	"sync"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/prettytechnical-tools/logger"
)

// Cache is an in-memory storage to cache data.
type Cache interface {
	Get(key string) ([]byte, error)
	Set(key string, value any, expiration time.Duration) error
	Drop(key string) error
	Incr(key string) error
	Exists(key string) (bool, error)
	Subscribe(channel string) <-chan *redis.Message
	Publish(channel string, msg []byte) error
	Ping() error
}

var (
	redisClient Cache
	redisOnce   sync.Once
)

// Client is the system cache.
type client struct {
	Client *redis.Client
}

// NewRedisClient returns a new redis client.
func NewRedisClient(address, password string) Cache {
	log := logger.New("redis", true)
	redisOnce.Do(func() {
		c := redis.NewClient(&redis.Options{
			Addr:     address,
			Password: password,
			DB:       0,
		})

		if err := c.Ping().Err(); err != nil {
			log.Errorf("cannot create REDIS connection: %v", err)
			panic(err)
		}

		redisClient = &client{
			Client: c,
		}
	})
	return redisClient
}

func (c *client) Get(key string) ([]byte, error) {
	return c.Client.Get(key).Bytes()
}

// Set save a value with key.
func (c *client) Set(key string, value any, expiration time.Duration) error {
	return c.Client.Set(key, value, expiration).Err()
}

// Drop remove a value with key.
func (c *client) Drop(key string) error {
	return c.Client.Del(key).Err()
}

// Incr remove a value with key.
func (c *client) Incr(key string) error {
	return c.Client.Incr(key).Err()
}

// Exists determines if a given key exists
func (c *client) Exists(key string) (bool, error) {
	e, err := c.Client.Exists(key).Result()
	if err != nil {
		return false, err
	}
	return e == 1, nil
}

// Ping checks if the connection is alive.
func (c *client) Ping() error {
	return c.Client.Ping().Err()
}

// Subscribe checks if the connection is alive.
func (c *client) Subscribe(channel string) <-chan *redis.Message {
	sub := c.Client.Subscribe(channel)

	return sub.Channel()
}

// Publish sends a message to a channel.
func (c *client) Publish(channel string, msg []byte) error {
	return c.Client.Publish(channel, msg).Err()
}
